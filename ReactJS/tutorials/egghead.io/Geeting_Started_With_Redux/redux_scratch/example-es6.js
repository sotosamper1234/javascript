// REDUCER
const counter = (state = 0, action) => {
  switch (action.type) {
    case 'INCREMENT':
      return state + 1;
    case 'DECREMENT':
      return state - 1;
    default: 
      return state;
  }
}

// ---------------------------------------
// REDUX FROM SCRATCH

const createStore = (reducer) => {
  let state;
  let listeners = [];

  const getState = () => state;

  const dispatch = (action) => {
    state = reducer(state, action);
    listeners.forEach(listener => listener());
  };

  const subscribe = (listener) => {
    listeners.push(listener);
    return () => {
      listeners = listeners.filter(l => l !== listener);
    }
  };

  dispatch({});

  return { getState, dispatch, subscribe };
};

// ---------------------------------------
// APP

const LIMIT_COUNTER = 10;
const store = createStore(counter);

const render = () => {
  document.body.innerText = store.getState();
};

const unsubscribe = store.subscribe(render);
render();

const checkCountOfListener = (state, action, unsubscriber, limit = LIMIT_COUNTER) => {
  return (state === limit) ? unsubscriber : action;
}

document.addEventListener('click', () => {
  let state = store.getState();
  let action  = store.dispatch.bind(null, { type: 'INCREMENT' });
  checkCountOfListener(state, action, unsubscribe)();
});


