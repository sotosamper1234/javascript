import { products, cart } from '../reducers';

describe('products', () => {
  it('returns the initial state', () => {
    expect(products(undefined, {})).toEqual([]);
  });

  it('receives products', () => {
    const productList = [
      { id: 1, name: "Product 1", price: 100, image: "" }
    ];

    expect(
      products([], { type: "REPLACE_PRODUCTS", products: productList })
    ).toEqual(productList);
  })
});

describe('cart', () => {
  it('returns the initial state', () => {
    expect(cart(undefined, {})).toEqual([]);
  });

  it('add product', () => {
    const product = { id: 1, name: "Product 1", price: 100, image: "" };
    const cartList = [ product ];

    expect(
      cart([], { type: "ADD_TO_CART", product: product })
    ).toEqual(cartList);
  });

  it('remove product', () => {
    const product = { id: 1, name: "Product 1", price: 100, image: "" };
    const cartList = [ product ];
    
    expect(
      cart(cartList, { type: "REMOVE_FROM_CART", product: product })
    ).toEqual([])
  });
});