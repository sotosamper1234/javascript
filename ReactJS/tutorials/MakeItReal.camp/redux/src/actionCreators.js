import axios from 'axios';

const loadProducts = () => {
  return dispatch => {
    return axios.get("http://localhost:3001/products")
      .then(response => {
        dispatch({
          type: "REPLACE_PRODUCTS",
          products: response.data
        })
      })
      .catch(error => {
        dispatch({
          type: "REPLACE_PRODUCTS",
          products: [
            { id: 1, name: "Default product", price: 0, image: "" }
          ]
        })
      });
  };
}

const addToCart = product => {
  return {
    type: "ADD_TO_CART",
    product
  };
};

const removeFromCart = product => {
  return {
    type: "REMOVE_FROM_CART",
    product
  };
};

export { loadProducts, addToCart, removeFromCart };