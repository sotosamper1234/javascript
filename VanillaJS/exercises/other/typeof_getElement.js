var body = {}, title = {}, index, element, status;
body.element = document.getElementsByTagName('body')[0];

// Remove previous titles.
body.titles = body.element.getElementsByTagName('h1');
console.log(body.titles);
for(index = body.titles.length; index--;) {
  if(body.titles[index] && body.titles[index].parentElement) {
    body.titles[index].parentElement.removeChild(body.titles[index]);
  }
}


title.element = document.createElement('h1');
title.attr = document.createAttribute('id');
title.attr.value = 'main_title';
title.element.setAttributeNode(title.attr);
title.text = document.createTextNode('My title');
title.element.appendChild(title.text);
body.divs = body.element.getElementsByTagName('div');
body.element.insertBefore(title.element, body.divs[0]);


console.log(body.element);
element = body.element.getElementsByTagName('p');
console.log(typeof element);
console.log(element);

console.log(body.element);
element = document.getElementById('main_title1');
console.log(typeof element);
console.log(element);

(!element) ? status = 'element not found' : status = 'element found';
console.log(status);

