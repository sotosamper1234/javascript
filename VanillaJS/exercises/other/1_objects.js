(function(global) {

  var testObjects = {};
  
  
  testObjects.testProperties = function() {
    var man = {
      hands: 2,
      legs: 2,
      heads: 1
    };

    if(typeof Object.prototype.clone === "undefined") {
      Object.prototype.clone = function() {};
    }

    for (var i in man) {
      if(man.hasOwnProperty(i)) {
        console.log(i, ":", man[i]);
      }
      else {
        console.log(i, "!", man[i]);
      }
    }
  };
  
  
  function test_optional_params(a, b, option) {
    var option = option || null;
    console.log(a, b, option);
  }
  testObjects.testOptionalParams = function() {
    test_optional_params('a', 1);
    test_optional_params('a', 1, 'options');
  };
  
  global.testObjects = testObjects;
})(this);