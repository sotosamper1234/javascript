(function() {
    /*
     * Extensión de la funcionalidad básica del navegador.
     */
    
    
    
    /*
     * Esta función permite añadir multiples clases a un objeto.
     * http://stackoverflow.com/a/11116364
     */
    DOMTokenList.prototype.addMany = DOMTokenList.prototype.addMany || function(classes) {
        var array = classes.split(' ');
        for (var i = 0, length = array.length; i < length; i++) {
          this.add(array[i]);
        }
    }

    /*
     * Esta función permite añadir multiples clases a un objeto.
     * http://stackoverflow.com/a/11116364
     */
    DOMTokenList.prototype.removeMany = DOMTokenList.prototype.removeMany || function(classes) {
        var array = classes.split(' ');
        for (var i = 0, length = array.length; i < length; i++) {
          this.remove(array[i]);
        }
    }
})();