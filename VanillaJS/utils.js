(function(global) {
    'use strict';
    
    var utils = {};

    /*
     * Configuración predeterminada de la aplicación.
     */
    utils.settings = {
        base_url: location.protocol + '//' + location.hostname + '/javascript/',
    };
    
    
    
    /*
     * Inicializa la list con la funcionalidad de mostrar/ocultar sus secciones.
     * @param {HTMLElement} list Elemento HTML.
     * @param {boolean} hidden Determina la visibilidad inicial de la lista.
     */
    utils.initTree = function(list, hidden) {
        
        var hidden = hidden || false;
        var button, text, icon;
        
        icon = ((hidden) ? "+" : "-");
        button = document.createElement('button');
        text = document.createTextNode(icon);
        button.appendChild(text);
        button.classList.add('tree-button');
        list.appendChild(button);
        
        //console.log(button, ul);
        
        list.addEventListener('click', showOrHideTree);
    };
    
     function showOrHideTree(event) {
        event.stopPropagation();
        if(this.classList.contains('hidden')) {
            this.classList.remove('hidden');
        }
        else {
            this.classList.add('hidden');
        }
    }
    

    /*
    Agrega la url base a los enlaces de archivos con el atributo data-dinamic-link.
    */
    utils.setDinamicLink = function() {
        var links, i, href;
        links = document.querySelectorAll('a[href][data-dinamic-link]');
        for(i = links.length; i--;) {
            href = links[i].getAttribute('href');
            links[i].setAttribute('href', utils.settings.base_url + href);
        }
    };
    
    
    utils.insertList = function(elements, container, recursive) {
        var recursive = recursive || true,
            max = elements.length,
            max_recursive,
            ul, li, btn, icon, a, pre, text;
        
        for(var i = 0; i < max; i++) {
            
            if(typeof elements[i].list !== "undefined") {
                ul = document.createElement('ul');
                if(typeof elements[i].text !== "undefined") {
                    text = document.createTextNode(elements[i].text);
                    ul.appendChild(text);
                }
                if(typeof elements[i].class !== "undefined") {
                    ul.classList.addMany(elements[i].class);
                    
                    if(ul.classList.contains('tree') === true) {
                        utils.initTree(ul, ul.classList.contains('hidden'));
                    }
                }
                if(recursive === true) {
                   utils.insertList(elements[i].list, ul);
                }
                container.appendChild(ul);
            }
            else {
                li = document.createElement('li');
                if(typeof elements[i].text !== "undefined") {
                    text = document.createTextNode(elements[i].text);
                    li.appendChild(text);
                }
                if(typeof elements[i].class !== "undefined") {
                    li.classList.add(elements[i].class)
                }
                if(typeof elements[i].a !== "undefined") {
                    a = document.createElement('a');
                    
                    if(elements[i].a.dinamic_link === true) {
                        a.setAttribute('href', utils.settings.base_url + elements[i].a.href);
                    }
                    else {
                        a.setAttribute('href', elements[i].a.href);
                    }
                    text = document.createTextNode(elements[i].a.text);
                    a.appendChild(text);
                    li.appendChild(a);
                }
                if(typeof elements[i].statement !== "undefined") {
                    pre = document.createElement('pre');
                    //text = document.createTextNode(elements[i].statement);
                    //pre.appendChild(text);
                    pre.innerHTML = elements[i].statement;
                    li.appendChild(pre);
                }
                container.appendChild(li);
            }
        }
    };
    
    /*
    Inicialización del modulo.
    */
    utils.init = function() {
        utils.setDinamicLink();
    };

    global.Utils = utils;
})(this);