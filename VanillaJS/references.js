(function(global) {
    'use strict';
    
    var references;

    /*
    Configuración predeterminada de la aplicación.
    */
    references = {
        information: {
            container: '#sources_list',
            elements : [
                {
                    a: {
                        text: 'ECMAScript® 2015 Language Specification (Ecma international)',
                        href: 'http://www.ecma-international.org/ecma-262/6.0/index.html'
                    }
                },
                {
                     a: {
                        text : 'Web technology for developers - JavaScripts (Mozilla developer network)',
                        href: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript'
                    }
                },
                {
                    a: {
                        text : 'JavaScript - The Right Way',
                        href: 'http://jstherightway.org/'
                    }
                },
                {
                    a: {
                        text : 'Speaking JavaScript',
                        href: 'http://speakingjs.com/es5/index.html'
                    }
                },
                {
                    a: {
                        text : 'Eloquent JavaScript',
                        href: 'http://eloquentjavascript.net/'
                    }
                },
                {
                    a: {
                        text : 'Learning JavaScript Design Patterns',
                        href: 'https://addyosmani.com/resources/essentialjsdesignpatterns/book/'
                    }
                },
                {
                    a: {
                        text : 'JavaScript_best_practices (W3)',
                        href: 'https://www.w3.org/wiki/JavaScript_best_practices'
                    }
                }
            ]
        },
        exercises: {
            container: '#exercises_list',
            path: 'exercises/',
            blocks: [
                {
                    text: 'Eloquent javascript book exercises',
                    path: 'eloquentjavascript/',
                    class: 'tree',
                    list: [
                        {
                            text: 'Program structure',
                            path: '02_program_structure/',
                            class: 'tree hidden',
                            list: [
                                {
                                    a: {
                                        text : 'Looping a triangle',
                                        href: 'exercises/eloquentjavascript/02_program_structure/1_looping_triangle.html',
                                        dinamic_link: true
                                    }
                                },
                                {
                                    a: {
                                        text : 'FizzBuzz',
                                        href: 'exercises/eloquentjavascript/02_program_structure/2_fizz_buzz.html',
                                        dinamic_link: true
                                    }
                                },
                                {
                                    a: {
                                        text : 'Chess board',
                                        href: 'exercises/eloquentjavascript/02_program_structure/3_chess_board.html',
                                        dinamic_link: true
                                    }
                                }
                            ]
                        },
                        {
                            text: 'Functions',
                            path: '03_functions/',
                            class: 'tree hidden',
                            list: [
                                {
                                    a: {
                                        text : 'Minimum',
                                        href: 'exercises/eloquentjavascript/03_functions/1_minimun.html',
                                        dinamic_link: true
                                    }
                                },
                                {
                                    a: {
                                        text : 'Recursion',
                                        href: 'exercises/eloquentjavascript/03_functions/2_recursion.html',
                                        dinamic_link: true
                                    }
                                },
                                {
                                    a: {
                                        text : 'Bean counting',
                                        href: 'exercises/eloquentjavascript/03_functions/3_bean_counting.html',
                                        dinamic_link: true
                                    }
                                }
                            ]
                        },
                        {
                            text: 'Data Structures: Objects and Arrays',
                            path: '04_data_structures/',
                            class: 'tree hidden',
                            list: [
                                {
                                    a: {
                                        text : 'Example - The weresquirrel',
                                        href: 'exercises/eloquentjavascript/04_data_structures/0_example.html',
                                        dinamic_link: true
                                    }
                                },
                                {
                                    a: {
                                        text : 'The sum of a range',
                                        href: 'exercises/eloquentjavascript/04_data_structures/1_sum_of_range.html',
                                        dinamic_link: true
                                    }
                                },
                                {
                                    a: {
                                        text : 'Reversing an array',
                                        href: 'exercises/eloquentjavascript/04_data_structures/2_reversing_array.html',
                                        dinamic_link: true
                                    }
                                },
                                {
                                    a: {
                                        text : 'A list',
                                        href: 'exercises/eloquentjavascript/04_data_structures/3_list.html',
                                        dinamic_link: true
                                    }
                                },
                                {
                                    a: {
                                        text : 'Deep comparison',
                                        href: 'exercises/eloquentjavascript/04_data_structures/4_deep_comparison.html',
                                        dinamic_link: true
                                    }
                                }
                            ]
                        },
                        {
                            text: 'Higher-order Functions',
                            path: '05_higher_order_functions/',
                            class: 'tree hidden',
                            list: [
                                {
                                    a: {
                                        text : 'Example - Ancestors',
                                        href: 'exercises/eloquentjavascript/05_higher_order_functions/0_example.html',
                                        dinamic_link: true
                                    }
                                },
                                {
                                    a: {
                                        text : 'Flattening',
                                        href: 'exercises/eloquentjavascript/05_higher_order_functions/1_flattening.html',
                                        dinamic_link: true
                                    }
                                },
                                {
                                    a: {
                                        text : 'Mother-child age difference',
                                        href: 'exercises/eloquentjavascript/05_higher_order_functions/2_mother_child_age_difference.html',
                                        dinamic_link: true
                                    }
                                },
                                {
                                    a: {
                                        href: 'exercises/eloquentjavascript/05_higher_order_functions/3_historical_life_expectancy.html',
                                        text : 'Historcal life expectancy',
                                        dinamic_link: true
                                    }
                                },
                                {
                                    a: {
                                        href: 'exercises/eloquentjavascript/05_higher_order_functions/4_every_and_then_some.html',
                                        text : 'HistorcalEvery and then some',
                                        dinamic_link: true
                                    }
                                }
                            ]
                        },
                        {
                            text: 'The Secret Life of Objects',
                            path: '06_secret_life_objects/',
                            class: 'tree hidden',
                            list: [
                                {
                                    a: {
                                        text : 'Example - Laying out a table',
                                        href: 'exercises/eloquentjavascript/06_secret_life_objects/0_example.html',
                                        dinamic_link: true
                                    }
                                },
                                {
                                    a: {
                                        href: 'exercises/eloquentjavascript/06_secret_life_objects/1_vector_type.html',
                                        text : 'A vector type',
                                        dinamic_link: true
                                    }
                                },
                                {
                                    a: {
                                        href: 'exercises/eloquentjavascript/06_secret_life_objects/2_another_cell.html',
                                        text : 'Another cell',
                                        dinamic_link: true
                                    }
                                },
                                {
                                    a: {
                                        href: 'exercises/eloquentjavascript/06_secret_life_objects/3_sequence_interface.html',
                                        text : 'Sequence interface',
                                        dinamic_link: true
                                    }
                                }
                            ]
                        },
                        {
                            text: 'Project: Electronic Life',
                            path: '07_project_electronic_life/',
                            class: 'tree hidden',
                            list: [
                                {
                                    a: {
                                        text : 'Example - Electronic Life',
                                        href: 'exercises/eloquentjavascript/07_project_electronic_life/0_example.html',
                                        dinamic_link: true
                                    }
                                },
                                {
                                    a: {
                                        href: 'exercises/eloquentjavascript/07_project_electronic_life/1_artificial_stupidity.html',
                                        text : 'Artificial stupidity',
                                        dinamic_link: true
                                    }
                                },
                                {
                                    a: {
                                        href: 'exercises/eloquentjavascript/07_project_electronic_life/2_predators.html',
                                        text : 'Predators',
                                        dinamic_link: true
                                    }
                                }
                            ]
                        },
                        {
                            text: 'Bugs and Error Handling',
                            path: '08_bugs_and_error_handling/',
                            class: 'tree hidden',
                            list: [
                                {
                                    a: {
                                        text : 'Example',
                                        href: 'exercises/eloquentjavascript/08_bugs_and_error_handling/0_example.html',
                                        dinamic_link: true
                                    }
                                },
                                {
                                    a: {
                                        text : 'Retry',
                                        href: 'exercises/eloquentjavascript/08_bugs_and_error_handling/1_retry.html',
                                        dinamic_link: true
                                    }
                                },
                                {
                                    a: {
                                        text : 'The locked box',
                                        href: 'exercises/eloquentjavascript/08_bugs_and_error_handling/2_the_locked_box.html',
                                        dinamic_link: true
                                    }
                                }
                            ]
                        },
                        {
                            text: 'Regular Expressions',
                            path: '09_regular_expressions/',
                            class: 'tree hidden',
                            list: [
                                {
                                    a: {
                                        text : 'Regexp golf',
                                        href: 'exercises/eloquentjavascript/09_regular_expressions/1_regexp_golf.html',
                                        dinamic_link: true
                                    },
                                    a: {
                                        text : 'Quoting style',
                                        href: 'exercises/eloquentjavascript/09_regular_expressions/2_quoting_style.html',
                                        dinamic_link: true
                                    },
                                    a: {
                                        text : 'Numbers again',
                                        href: 'exercises/eloquentjavascript/09_regular_expressions/3_numbers_again.html',
                                        dinamic_link: true
                                    }
                                }
                            ]
                        }
                        
                    ]
                }
            ]
        }
    };
    
    function showSourceReferences() {
        var container = document.querySelector(references.information.container);
        Utils.insertList(references.information.elements, container);
    }
    
    function showExercises() {
        var container = document.querySelector(references.exercises.container);
        Utils.insertList(references.exercises.blocks, container);
    }
    
    references.init = function() {
        showSourceReferences();
        showExercises();
    };
    
    global.References = references;
})(this);