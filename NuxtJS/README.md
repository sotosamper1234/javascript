# NuxtJS

> My impeccable Nuxt.js project

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production
$ npm run build

# launch server
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).

---

# PM2
## Build Setup

``` bash
# install pm2 globally
$ npm install pm2 -g

# launch server with pm2
$ npm run start-pm2

# stop server with pm2
$ npm run stop-pm2
```

More info in [pm2.io](https://pm2.io).

