/**
 * Checkout Session.
 */
const stripe = require('stripe')('sk_test_r2ulOcAKDyxkBlSmkjU2RgJD00EibWHTyf')

module.exports = function(app) {
  app.post('/create-checkout-session', async (req, res) => {
    const session = await stripe.checkout.sessions.create({
      line_items: [
        {
          price_data: {
            currency: 'usd',
            product_data: {
              name: 'T-shirt',
            },
            unit_amount: 2000,
          },
          quantity: 1,
        },
      ],
      mode: 'payment',
      success_url: 'http://127.0.0.1:8123/success',
      cancel_url: 'http://127.0.0.1:8123/cancel',
    })

    res.redirect(303, session.url)
  })
}
